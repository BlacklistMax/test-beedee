import React, { useEffect, useState } from 'react';
import { View, Text, Button } from 'react-native';
import { fetchRandomQuestions } from '../services/api'; // Implement this function to fetch random questions

type IQuestion = {
  text: string
  answers:IAnswers[]
}
type IAnswers = {
  text:string
}
const Quiz = () => {
  const [questions, setQuestions] = useState<IQuestion[]>([]);

  useEffect(() => {
    fetchQuestions();
  }, []);

  const fetchQuestions = async () => {
    try {
      const data = await fetchRandomQuestions();
      setQuestions(data);
    } catch (error) {
      console.error('Error fetching questions:', error);
    }
  };
  
  const handleAnswer = async (question:IQuestion, answer:IAnswers) => {
    console.log(question, answer);
  }

  return (
    <View>
      {questions.map((question, index) => (
        <View key={index}>
          <Text>{question.text}</Text>
          {question.answers.map((answer, idx) => (
            <Button key={idx} title={answer.text} onPress={() => handleAnswer(question, answer)} />
          ))}
        </View>
      ))}
    </View>
  );
};

export default Quiz;