import React from 'react';
import { View, Text } from 'react-native';

const Leaderboard = ({ scores }: {
  scores: [
    {
      name:string
      score: number
    }
  ]}) => {
  return (
    <View>
      <Text>Leaderboard</Text>
      {scores.map((score, index) => (
        <Text key={index}>{score.name}: {score.score}</Text>
      ))}
    </View>
  );
};

export default Leaderboard;