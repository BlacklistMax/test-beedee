
export default function Case1() {
  const prefix = 'fl';
  const strs = ["dog","racecar","car"];
  const longestCommonPrefix = (strs: string[]): string => {
    if (strs.length === 0) return '';

    let prefix = strs[0];

    for (let i = 1; i < strs.length; i++) {
      while (strs[i].indexOf(prefix) !== 0) {
        prefix = prefix.substring(0, prefix.length - 1);
        if (prefix === '') return '';
      }
    }

    return prefix;
  };

  return (
    <>
      <p>Input: strs = {JSON.stringify(strs)}</p>
      <p>Output: {longestCommonPrefix(strs)}</p>
    </>
  )
}